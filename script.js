const numbers = document.querySelectorAll('.operand');
const operators = document.querySelectorAll('.operator');
const clearButton = document.querySelector('.clear');
const deleteButton = document.querySelector('.delete');
const display = document.querySelector('.display');
const equalsButton = document.querySelector('.equals');
const buttons = document.querySelectorAll('button');
const minus = document.querySelector('.minus');

let negativeNumber = '';
let firstNumber = '';
let firstOperator = '';
let secondNumber = '';
let secondOperator = '';
let result = 0;

// Functions for basic math operators
function addItems(item1, item2) {
    return item1 + item2;
}

function substractItems(item1, item2) {
    return item1 - item2;
}

function multiplyItems(item1, item2) {
    return item1 * item2;
}

function divideItems(item1, item2) {
    if (item2 === 0) {
        return 'not happening, buddy'
    } else {
        return item1 / item2;
    }
}

// Function 'operate' that takes an operator and 2 numbers, then calls one of the math functions above
function operate(number1, operator, number2) {
    number1 = parseFloat(number1);
    number2 = parseFloat(number2);
    if (operator === '+') {
        result = addItems(number1, number2);
    }
    if (operator === '-') {
        result = substractItems(number1, number2);
    }
    if (operator === '×') {
        result = multiplyItems(number1, number2);
    }
    if (operator === '÷') {
        result = divideItems(number1, number2);
    }
}

// Function related to a negative first number
minus.addEventListener('click', addNegativeNumber);

function addNegativeNumber() {
    if(firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
        if (this.innerText === '-' && negativeNumber.includes('-')) return
        negativeNumber = this.innerText;
        display.innerText = negativeNumber;
    }
}

// Functions related to first operand
numbers.forEach((item) => {
    item.addEventListener('click', updateFirstOperand)
});

function updateFirstOperand() {
    if(firstOperator === '' && secondNumber === '' && secondOperator === '') {
        if (this.innerText === '.' && firstNumber.includes('.')) return
        firstNumber += this.innerText;
        display.innerText = negativeNumber + firstNumber;
    }
}

// Functions related to the first operator
operators.forEach((item) => {
    item.addEventListener('click', updateFirstOperator)
});

function updateFirstOperator() { 
    if (firstNumber !== '' && secondNumber === '' && this.innerText === '=') return
    if (firstNumber !== '' && secondNumber === '' && secondOperator === '') {
        firstOperator = this.innerText;
        display.innerText = negativeNumber + firstNumber + firstOperator;
    }
}

// Functions related to the second operand
numbers.forEach((item) => {
    item.addEventListener('click', updateSecondOperand)
})

function updateSecondOperand() {
    if (firstNumber !== '' && firstOperator !== '' && secondOperator === '') {
        if (this.innerText === '.' && secondNumber.includes('.')) return
        secondNumber += this.innerText;
        display.innerText = negativeNumber + firstNumber + firstOperator + secondNumber;
    }
}

// Functions related to the second operator
operators.forEach((item) => {
    item.addEventListener('click', updateSecondOperator)
});

function updateSecondOperator() {
    if (firstNumber !== '' && secondNumber !== '' && firstOperator !== '') {
        secondOperator = this.innerText;
        calculate();
        firstNumber = result;
        if(result === 'not happening, buddy') {
            display.innerText = result;
            return
        }
        if(isNaN(result)) {
            display.innerText = 'I need numbers';
            // In a loop to be able to clear the screen upon pressing Escape
            if (this.innerText != 'C') {
                return
            }
        }
        if(this.innerText === '=') {
            firstOperator = '';
        } else {
            firstOperator = secondOperator;
        }
        secondNumber = '';
        secondOperator = '';
        negativeNumber = '';
        display.innerText = result+firstOperator;
    }
}

// Calculator function
function calculate() {
    if (firstNumber !== '' && firstOperator !== '' && secondNumber !== '' && secondOperator !== '') {
        operate((negativeNumber + firstNumber), firstOperator, secondNumber);
    }
}

// Clear button
clearButton.addEventListener('click', clearDisplay);

function clearDisplay() {
    negativeNumber = '';
    firstNumber = '';
    firstOperator = '';
    secondNumber = '';
    secondOperator = '';
    result = 0;
    display.innerText = result;
}

// Delete button
deleteButton.addEventListener('click', deleteText);

function deleteText() {
    if(negativeNumber !=='' && firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
        negativeNumber = negativeNumber.slice(0, -1);
        display.innerText = negativeNumber;
    }
    if(firstOperator === '' && secondNumber === '' && secondOperator === '') {
        firstNumber = firstNumber.slice(0, -1);
        display.innerText = negativeNumber + firstNumber;
    }
    if(firstNumber !== '' && secondNumber === '' && secondOperator === '') {
        firstOperator = '';
        display.innerText = negativeNumber + firstNumber;
    }
    if (firstNumber !== '' && firstOperator !== '' && secondOperator === '') {
        secondNumber = secondNumber.slice(0, -1);
        display.innerText = negativeNumber + firstNumber + firstOperator + secondNumber;
    }
    if(negativeNumber === '' && firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
        display.innerText = result;
    }
}

// Functions that allow keyboard input
window.addEventListener('keydown', function(e) {
    // 'if' close relate to negativeNumber
    if(firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
        if (e.key === '-' && negativeNumber.includes('-')) return
        if (e.key === '-') {negativeNumber += e.key;}
        display.innerText = negativeNumber;
    }
    // 'if' close related to first operand
    if(firstOperator === '' && secondNumber === '' && secondOperator === '') {
        if (e.key === '.' && firstNumber.includes('.')) return
        if (e.key === '0') {firstNumber += e.key;}
        if (e.key === '1') {firstNumber += e.key;}
        if (e.key === '2') {firstNumber += e.key;} 
        if (e.key === '3') {firstNumber += e.key;}
        if (e.key === '4') {firstNumber += e.key;}
        if (e.key === '5') {firstNumber += e.key;}
        if (e.key === '6') {firstNumber += e.key;}
        if (e.key === '7') {firstNumber += e.key;} 
        if (e.key === '8') {firstNumber += e.key;}
        if (e.key === '9') {firstNumber += e.key;}
        if (e.key === '.') {firstNumber += e.key;}
        display.innerText = negativeNumber + firstNumber;
    }
    // 'If' close related to the first operator
    if (firstNumber !== '' && secondNumber === '' && secondOperator === '') {      
        if (e.key === '+') {firstOperator = e.key;}
        if (e.key === '-') {firstOperator = e.key;}
        if (e.key === '*') {firstOperator = '×';}
        if (e.key === '/') {firstOperator = '÷';}
        display.innerText = negativeNumber + firstNumber + firstOperator;
        e.preventDefault();
    }
    // 'If' close relate to the second operand
    if (firstNumber !== '' && firstOperator !== '' && secondOperator === '') {
        if (e.key === '.' && secondNumber.includes('.')) return
        if (e.key === '0') {secondNumber += e.key;}
        if (e.key === '1') {secondNumber += e.key;}
        if (e.key === '2') {secondNumber += e.key;} 
        if (e.key === '3') {secondNumber += e.key;}
        if (e.key === '4') {secondNumber += e.key;}
        if (e.key === '5') {secondNumber += e.key;}
        if (e.key === '6') {secondNumber += e.key;}
        if (e.key === '7') {secondNumber += e.key;} 
        if (e.key === '8') {secondNumber += e.key;}
        if (e.key === '9') {secondNumber += e.key;}
        if (e.key === '.') {secondNumber += e.key;}
        display.innerText = negativeNumber + firstNumber + firstOperator + secondNumber;
    }
    // 'If' close related to the second operator 1/2
    if (firstNumber !== '' && secondNumber !== '' && firstOperator !== '') {
        if (e.key === '+') {secondOperator = e.key;}
        if (e.key === '-') {secondOperator = e.key;}
        if (e.key === '*') {secondOperator = '×';}
        if (e.key === '/') {secondOperator = '÷';}
        if (e.key === '=') {secondOperator = e.key;}
        if (e.key === 'Enter') {secondOperator = '=';}
        e.preventDefault();
    }
    // 'If' close related to the second operator 2/2
    if (firstNumber !== '' && firstOperator !== '' && secondNumber !== '' && secondOperator !== '') {
        calculate();
        firstNumber = result;
        if(result === 'not happening, buddy') {
            display.innerText = result;
            // In a loop to be able to clear the screen upon pressing Escape
            if (e.key != 'Escape') {
                return
            }
        }
        if(isNaN(result)) {
            display.innerText = 'I need numbers';
            // In a loop to be able to clear the screen upon pressing Escape
            if (e.key != 'Escape') {
                return
            }
        }
        if(e.key === '=' || e.key === 'Enter') {
            firstOperator = '';
        } else {
            firstOperator = secondOperator;
        }
        secondNumber = '';
        secondOperator = '';
        negativeNumber = ''
        display.innerText = result+firstOperator;
    }
    // 'If' close related to Escape key/ Clear function
    if (e.key === 'Escape') {
        negativeNumber = '';
        firstNumber = '';
        firstOperator = '';
        secondNumber = '';
        secondOperator = '';
        result = 0;
        display.innerText = result;
    }
    // 'If' close related to Backspace key/ delete last character function
    if (e.key === 'Backspace') {
        if(negativeNumber !=='' && firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
            negativeNumber = negativeNumber.slice(0, -1);
            display.innerText = negativeNumber;
        }
        if(firstOperator === '' && secondNumber === '' && secondOperator === '') {
            firstNumber = firstNumber.slice(0, -1);
            display.innerText = negativeNumber + firstNumber;
        }
        if(firstNumber !== '' && secondNumber === '' && secondOperator === '') {
            firstOperator = '';
            display.innerText = negativeNumber + firstNumber;
        }
        if (firstNumber !== '' && firstOperator !== '' && secondOperator === '') {
            secondNumber = secondNumber.slice(0, -1);
            display.innerText = negativeNumber + firstNumber + firstOperator + secondNumber;
        }
        if(negativeNumber === '' && firstNumber === '' && firstOperator === '' && secondNumber === '' && secondOperator === '') {
            display.innerText = result;
        }
    }
    e.preventDefault();
});

// Screen buttons activate on key input
window.addEventListener('keydown', function(e) {
    if (e.key === '0') {document.querySelector('.zero').classList.toggle('active-key');}
    if (e.key === '1') {document.querySelector('.one').classList.toggle('active-key');}
    if (e.key === '2') {document.querySelector('.two').classList.toggle('active-key');} 
    if (e.key === '3') {document.querySelector('.three').classList.toggle('active-key');}
    if (e.key === '4') {document.querySelector('.four').classList.toggle('active-key');}
    if (e.key === '5') {document.querySelector('.five').classList.toggle('active-key');}
    if (e.key === '6') {document.querySelector('.six').classList.toggle('active-key');}
    if (e.key === '7') {document.querySelector('.seven').classList.toggle('active-key');} 
    if (e.key === '8') {document.querySelector('.eight').classList.toggle('active-key');}
    if (e.key === '9') {document.querySelector('.nine').classList.toggle('active-key');}
    if (e.key === '.') {document.querySelector('.period').classList.toggle('active-key');}
    if (e.key === '+') {document.querySelector('.plus').classList.toggle('active-key');}
    if (e.key === '-') {document.querySelector('.minus').classList.toggle('active-key');}
    if (e.key === '*') {document.querySelector('.times').classList.toggle('active-key');}
    if (e.key === '/') {document.querySelector('.divide').classList.toggle('active-key');}
    if (e.key === '=') {document.querySelector('.equals').classList.toggle('active-key');}
    if (e.key === 'Enter') {document.querySelector('.equals').classList.toggle('active-key');}
    if (e.key === 'Escape') {document.querySelector('.clear').classList.toggle('active-key');}
    if (e.key === 'Backspace') {document.querySelector('.delete').classList.toggle('active-key');}
});

window.addEventListener('keyup', function(e) {
    if (e.key === '0') {document.querySelector('.zero').classList.toggle('active-key');}
    if (e.key === '1') {document.querySelector('.one').classList.toggle('active-key');}
    if (e.key === '2') {document.querySelector('.two').classList.toggle('active-key');} 
    if (e.key === '3') {document.querySelector('.three').classList.toggle('active-key');}
    if (e.key === '4') {document.querySelector('.four').classList.toggle('active-key');}
    if (e.key === '5') {document.querySelector('.five').classList.toggle('active-key');}
    if (e.key === '6') {document.querySelector('.six').classList.toggle('active-key');}
    if (e.key === '7') {document.querySelector('.seven').classList.toggle('active-key');} 
    if (e.key === '8') {document.querySelector('.eight').classList.toggle('active-key');}
    if (e.key === '9') {document.querySelector('.nine').classList.toggle('active-key');}
    if (e.key === '.') {document.querySelector('.period').classList.toggle('active-key');}
    if (e.key === '+') {document.querySelector('.plus').classList.toggle('active-key');}
    if (e.key === '-') {document.querySelector('.minus').classList.toggle('active-key');}
    if (e.key === '*') {document.querySelector('.times').classList.toggle('active-key');}
    if (e.key === '/') {document.querySelector('.divide').classList.toggle('active-key');}
    if (e.key === '=') {document.querySelector('.equals').classList.toggle('active-key');}
    if (e.key === 'Enter') {document.querySelector('.equals').classList.toggle('active-key');}
    if (e.key === 'Escape') {document.querySelector('.clear').classList.toggle('active-key');}
    if (e.key === 'Backspace') {document.querySelector('.delete').classList.toggle('active-key');}
});