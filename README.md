# Calculator

Use the calculator live at [https://yoadrian.gitlab.io/calculator/](https://yoadrian.gitlab.io/calculator/)

## About the Project

The last [project](https://www.theodinproject.com/lessons/foundations-calculator) of The Odin Project's Foundations course.
- handles basic math functions
- built to allow chaining the result to other operators
- messages the user when attempting to divide by 0
- handles floating point numbers
- supports keyboard input
    - for all numbers and operators
    - Esc key for clearing the display
    - Backspace for deleting the last character
    - Enter or '=' to show the result of an operation
- displays well on mobile